package practica8.facci.practica8;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class actividadMemoriaInterna extends AppCompatActivity implements View.OnClickListener{

    EditText cajaCedula, cajaNombre, cajaApellidos;
    TextView cajaDatos;
    Button botonLeer, botonEscribir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_memoria_interna);

        cajaCedula = (EditText) findViewById(R.id.txtCedulaMI);
        cajaNombre = (EditText) findViewById(R.id.txtNombresMI);
        cajaApellidos = (EditText) findViewById(R.id.txtApellidosMI);
        cajaDatos = (TextView)findViewById(R.id.tvDatosMI);

        botonEscribir = (Button)findViewById(R.id.btnEscribirMI);
        botonLeer = (Button)findViewById(R.id.btnLeerMI);

        botonEscribir.setOnClickListener(this);
        botonLeer.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEscribirMI:
                try{
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(cajaCedula.getText().toString()+", "+ cajaApellidos.getText().toString()+", "+cajaNombre.getText().toString() +";");
                    escritor.close();
                    Toast.makeText(this,"se ah guardado los datos",Toast.LENGTH_LONG).show();
                    cajaDatos.setText("");
                }catch (Exception ex){
                    Log.e("Archivo MI","Error en el archivo de escritura");
                }
                break;

            case R.id.btnLeerMI:
                try{
                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String datos = lector.readLine();
                    String [] listaPersonas = datos.split(";");
                    for (int i = 0; i < listaPersonas.length; i++){
                        cajaDatos.append(listaPersonas[i].split(",")[0]+", " +
                                        listaPersonas[i].split(",")[1]+", " +
                                    listaPersonas[i].split(",")[2] +"\n ");
                    }
                    cajaCedula.setText("");
                    cajaNombre.setText("");
                    cajaApellidos.setText("");
                    lector.close();
                }catch (Exception e){
                    Log.e("Archivo MI", "Error en la lectura del archivo"+e.getMessage());

                }
                break;
        }

    }
}
